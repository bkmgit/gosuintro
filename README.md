# GOSUintro

Introduction to [Gosu](https://www.libgosu.org/) library 

Presentation for [Nairuby](https://www.meetup.com/Nairuby/)

Slides can be viewed at [https://bkmgit.gitlab.io/gosuintro/](https://bkmgit.gitlab.io/gosuintro/)